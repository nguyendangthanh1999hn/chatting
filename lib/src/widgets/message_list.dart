import 'package:flutter/material.dart';

import 'package:flutter_app_chatting/src/widgets/message_item.dart';
import 'package:flutter_app_chatting/src/models/message.dart';

class MessageList extends StatelessWidget {
  final List<Message> messages;

  MessageList({this.messages});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: messages.length,
      itemBuilder: (context, index) => MessageItem(message: messages[index]),
    );
  }
}
